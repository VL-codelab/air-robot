local math = require('tools/math');

local order = {};
local sfx = "9";

function submit(orderType, oper, account, ticker, classCode, purchasePrice, quantity, cc, precision)
    if (oper == 'Buy') then
        operation = 'B';
    end;

    if (oper == 'Sell') then
        operation = 'S';
    end;

    price = tostring(math.round(purchasePrice, precision));
    if (orderType == 'M') then
        price = '0';
    end;

    local transaction = {
        ['TRANS_ID']    = tostring(os.date("%d%H%M") .. math.random(1, 4) .. math.random(4, 8) .. sfx) ,
        ['ACTION']      = 'NEW_ORDER',
        ['CLASSCODE']   = classCode,
        ['SECCODE']     = ticker,
        ['OPERATION']   = operation,
        ['TYPE']        = orderType,
        ['QUANTITY']    = tostring(quantity),
        ['ACCOUNT']     = account,
        ['PRICE']       = price,
        ['CLIENT_CODE'] = cc,
    }

    local result = sendTransaction(transaction);

    if (string.len(result) ~= 0) then
        message(result);
        return false;
    end;

    return true;
end;

function delete(ticker, account, classCode, transId, orderId, cc)
    local transaction = {
        ['ACTION']       = "KILL_ORDER", 
        ['CLASSCODE']    = classCode,
        ['SECCODE']      = ticker,
        ['ACCOUNT']      = account,
        ['CLIENT_CODE']  = cc,
        ['TYPE']         = "M",
        ['ORDER_KIND']   = "SIMPLE_ORDER",
        ['TRANS_ID']     = tostring(transId),
        ['ORDER_KEY']    = tostring(orderId)
    }

    local result = sendTransaction(transaction);

    if (string.len(result) ~= 0) then
        message(result);
        return false;
    end;

    return true;
end;

function checkTransId(transId)
    local suffix = string.sub(transId, string.len(transId));

    if (suffix == sfx) then 
        return true;
    end;

    return false;
end;

order = {
    ['submit'] = submit,
    ['delete'] = delete,
    ['checkTransId'] = checkTransId,
}

return order;