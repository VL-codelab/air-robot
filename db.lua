require("os");
require("io");
json  = require("tools/json");

local db = {};
local log 		   = nil;
local slot 		   = nil;
local transactions = nil;

function addTransaction(order)
	local str = json.encode(order);
	transactions:write(os.date("%Y-%m-%d %H:%M:%S") .. ";" .. str .. "; \n");
end;

function lockSlot(state)
	unlockSlot();
	local str = json.encode(state);
	slot:write(str);
end;

function restoreSlotState()
    local content = slot:read("*a");

    if (content == nil or content == '') then
    	return {};
    end;

	local obj = json.decode(content);
	
	return obj;
end;

function unlockSlot()
	slot:close();
	local path = getScriptPath() .. "\\db\\slots\\" .. slotId .. ".txt";
	os.remove(path);
	
	slot = io.open(path, "r+");
    
    if slot == nil then 
      slot = io.open(path, "w");
      slot:close();
      slot = io.open(path, "r+");
    end;
end;

function log(str)
	log:write(os.date("%Y-%m-%d %H:%M:%S") .. " " .. str);
end;

function open(slotId)
	-- Пытается открыть файл в режиме "чтения/записи"
    log = io.open(getScriptPath() .. "\\db\\logs\\" .. slotId .. ".txt", "r+");
    -- Если файл не существует
    if log == nil then 
      -- Создает файл в режиме "записи"
      log = io.open(getScriptPath() .. "\\db\\logs\\" .. slotId .. ".txt", "w");
      -- Закрывает файл
      log:close();
      -- Открывает уже существующий файл в режиме "чтения/записи"
      log = io.open(getScriptPath() .. "\\db\\logs\\" .. slotId .. ".txt", "r+");
    end;

    slot = io.open(getScriptPath() .. "\\db\\slots\\" .. slotId .. ".txt", "r+");
    
    if slot == nil then 
      slot = io.open(getScriptPath() .. "\\db\\slots\\" .. slotId .. ".txt", "w");
      slot:close();
      slot = io.open(getScriptPath() .. "\\db\\slots\\" .. slotId .. ".txt", "r+");
    end;

    transactions = io.open(getScriptPath() .. "\\db\\transactions\\" .. slotId .. ".csv", "r+");
    
    if transactions == nil then 
      transactions = io.open(getScriptPath() .. "\\db\\transactions\\" .. slotId .. ".csv", "w");
      transactions:close();
      transactions = io.open(getScriptPath() .. "\\db\\transactions\\" .. slotId .. ".csv", "r+");
    end;

end;

function close()
	log:close();
	slot:close();
	transactions:close();
end;

db = {
	['addTransaction']    = addTransaction,
	['lockSlot']          = lockSlot,
	['restoreSlotState']  = restoreSlotState,
	['unlockSlot']     	  = unlockSlot,
	['log']               = log,
	['open']              = open,
	['close']             = close,
};

return db;