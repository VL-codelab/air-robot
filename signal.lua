local math = require('tools/math');
local dbg = require('tools/dbg');
local signal = {};

function getIndicators(ticker, indicator, indicator2) 
    local candleNum = getNumCandles(ticker);
        
    local candles, candlesCount, candlesGraphName = getCandlesByIndex(ticker, 0, candleNum-2, 2);
            
    candle     = candles[1];
    prevCandle = candles[0];

    local candleNum = getNumCandles(indicator);

    e_up, e_up_count, e_up_name = getCandlesByIndex(indicator, 1, candleNum-2, 2);
    e_down, e_down_count, e_down_name = getCandlesByIndex(indicator, 2, candleNum-2, 2);

    e_rsi, e_rsi_count, e_rsi_name = getCandlesByIndex(indicator2, 0, candleNum-2, 2);

    return candle, prevCandle, e_up, e_down, e_rsi;
end;

function getCandles(ticker, indicator, indicator2)
    candle, prevCandle, tmp1, tmp2, tmp3 = getIndicators(ticker, indicator, indicator2);

    return candle, prevCandle;
end;

function getProfit(purchasePrice, operation, precision)
    if (operation == 'B') then
        profitPrice = purchasePrice + math.round(purchasePrice * 0.0015, precision);
    end;

    if (operation == 'S') then
        profitPrice = purchasePrice - math.round(purchasePrice * 0.0015, precision);
    end;

    return profitPrice;
end;

function getEnter(ticker, indicator, indicator2, precision)
    candle, prevCandle, e_upline, e_downline, e_rsi = getIndicators(ticker, indicator, indicator2);

    prevOpen  = prevCandle.open; 
    prevClose = prevCandle.close;
    prevHigh  = prevCandle.high;
    prevLow   = prevCandle.low;

    open  = candle.open;
    close = candle.close;
    high  = candle.high;
    low   = candle.low;

    upline      = e_upline[1].close;
    prevUpline  = e_upline[0].close;

    downline      = e_downline[1].close;
    prevDownline  = e_downline[0].close;

    rsi = e_rsi[1].close;

    if (rsi < 25 and prevDownline > prevClose and downline < close and open < close) then
        stopPrice   = getStop(close, 'B', precision);
        profitPrice = getProfit(close, 'B', precision);
        return close, 'B', stopPrice, profitPrice;
    end;

    if (rsi > 75 and prevUpline < prevClose and close < upline and open > close) then
        stopPrice    = getStop(close, 'S', precision);
        profitPrice  = getProfit(close, 'S', precision);
        return close, 'S', stopPrice, profitPrice;
    end;

    return nil, nil;
end;

function getStop(purchasePrice, operation, precision)
    
    stopPrice = nil;

    if (operation == 'B') then
        stopPrice = purchasePrice - math.round(purchasePrice * 0.05, precision);
    end;

    if (operation == 'S') then
        stopPrice = purchasePrice + math.round(purchasePrice * 0.05, precision);
    end;

    return stopPrice;
end;

function checkStopPrice(ticker, indicator, indicator2, stopPrice, operation)
    -- candle, prevCandle = getCandles(ticker, indicator, indicator2);

    -- close = candle.close;

    -- if (operation == 'B') then
    --     if (stopPrice > close) then
    --         return close;
    --     end;
    -- end;

    -- if (operation == 'S') then
    --     if (stopPrice < close) then
    --         return close;
    --     end;
    -- end;

    return nil;
end;

signal = {
    ['getEnter']       = getEnter,
    ['getStop']        = getStop,
    ['checkStopPrice'] = checkStopPrice,
    ['getIndicators']  = getIndicators,
    ['getProfit']      = getProfit,
    ['getCandles']     = getCandles,
};

return signal;