local math = require('math');

function math.round(num, idp)
  local mult = 10 ^ (idp or 0);
  return math.floor(num * mult + 0.5) / mult;
end;

return math;
