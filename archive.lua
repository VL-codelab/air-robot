if (enterOrder['orderId'] ~= nil and takeProfitOrder['orderId'] ~= nil and enterOrder['status'] == 'complete' and takeProfitOrder['status'] == 'created') then
        local stopPrice = signal.getStop(enterOrder['purchasePrice'], enterOrder['operation'], candle.close);

        if (stopPrice ~= nil and enterOrder['operation'] == 'B') then
            order.delete(takeProfitOrder['ticker'], object.account, object.classCode, takeProfitOrder['transId'], takeProfitOrder['orderId'], object.clientCode);
            sleep(5000);
            order.submit('Sell', object.account, object.ticker, object.classCode, stopPrice, object.quantity * 2, object.clientCode, object.precision);
            sleep(5000);
        end;
    end;

        if (exitOrder['orderId'] == nil and stopOrder['orderId'] ~= nil and stopOrder['status'] == 'complete') then
        local purchasePrice = signal.getProfit(stopOrder['purchasePrice'], 'S');
        order.submit('Buy', object.account, object.ticker, object.classCode, purchasePrice, object.quantity, object.clientCode, object.precision);
        sleep(5000);
    end;

        if (prev['exitOrder'] ~= nil and prev['exitOrder'].orderId == params['orderId']) then
        return false;
    end;




        if (stopOrder['orderId'] ~= nil and stopOrder['orderId'] ~= params['orderId'] and exitOrder['orderId'] == nil) then
            db.unlockSlot();
            exitOrder = params;
            db.lockSlot(enterOrder);
            db.addTransaction(params);
        end;

        if (exitOrder['orderId'] ~= nil and exitOrder['orderId'] == params['orderId']) then
            exitOrder = params;
            db.addTransaction(params);
        end;

        if (exitOrder['orderId'] ~= nil and exitOrder['orderId'] == params['orderId'] and exitOrder['status'] == 'complete') then
            db.unlockSlot();
            db.addTransaction(params);
            reset();
        end;

        
    if (exitOrder['orderId'] ~= nil) then
        local copy = arr.deepcopy(exitOrder);
        prev['exitOrder'] = copy;
    end;