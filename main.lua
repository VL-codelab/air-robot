require("os");
dbg    = require("tools/dbg");
arr    = require("tools/arr");
order  = require("order");
signal = require("signal");
db     = require("db");

slotId = 'slot2';

isRun = true;

objects = {
    {
         ["classCode"]  = 'SPBFUT',
         ["ticker"]     = 'SRH8',
         ["graph"]      = 'graph_sber',
         ["indicator"]  = 'env_sber',
         ["indicator2"] = 'rsi_sber',
         ["quantity"]   = 5,
         ["precision"]  = 0,
         ["account"]    = 'SPBFUTD9QAP',
         ["clientCode"] = 'SPBFUTD9QAP',
    },
    {
         ["classCode"]  = 'SPBFUT',
         ["ticker"]     = 'VBH8',
         ["graph"]      = 'graph_vtb',
         ["indicator"]  = 'env_vtb',
         ["indicator2"] = 'rsi_vtb',
         ["quantity"]   = 30,
         ["precision"]  = 0,
         ["account"]    = 'SPBFUTD9QAP',
         ["clientCode"] = 'SPBFUTD9QAP',
    },
    {
         ["classCode"]  = 'SPBFUT',
         ["ticker"]     = 'GZH8',
         ["graph"]      = 'graph_gazp',
         ["indicator"]  = 'env_gazp',
         ["indicator2"] = 'rsi_gazp',
         ["quantity"]   = 10,
         ["precision"]  = 0,
         ["account"]    = 'SPBFUTD9QAP',
         ["clientCode"] = 'SPBFUTD9QAP',
    },
}

minutes = {
    ["04"] = true,
    ["09"] = true,
    ["14"] = true,
    ["19"] = true,
    ["24"] = true,
    ["29"] = true,
    ["34"] = true,
    ["39"] = true,
    ["44"] = true,
    ["49"] = true,
    ["54"] = true,
    ["59"] = true,
};

enterOrder = {};
takeProfitOrder  = {};
stopOrder  = {};

globalProfitPrice = nil;
globalStopPrice   = nil;
globalPurchasePrice = nil;

prev = {};

lastCandleHM = nil;
lastTicker = nil;

function main()
    while isRun do
        for index, object in ipairs(objects) do
            if (enterOrder['orderId'] == nil or enterOrder['ticker'] == object.ticker) then
                process(object);
            end;
        end;
        sleep(5000);
    end;
end;

function process(object)

    local candle, prevCandle = signal.getCandles(object.graph, object.indicator, object.indicator2);

    local candleHM = candle.datetime.hour .. candle.datetime.min;

    currentMinute = os.date('%M');
    currentHour   = os.date('%H');
    currentSecond = os.date('%S');

    if ((minutes[tostring(currentMinute)]) and (tostring(currentSecond) > "54")) then
        if (enterOrder['orderId'] == nil) then

            local purchasePrice, operation, stopPrice, profitPrice = signal.getEnter(object.graph, object.indicator, object.indicator2, object.precision);

            globalProfitPrice   = profitPrice;
            globalStopPrice     = stopPrice;
            globalPurchasePrice = purchasePrice;

            if (purchasePrice ~= nil and operation == 'B') then
                order.submit('M', 'Buy', object.account, object.ticker, object.classCode, purchasePrice, object.quantity, object.clientCode, object.precision);
                sleep(3000);
            end;

            if (purchasePrice ~= nil and operation == 'S') then
                order.submit('M', 'Sell', object.account, object.ticker, object.classCode, purchasePrice, object.quantity, object.clientCode, object.precision);
                sleep(3000);
            end;
            
        end;
    end;

    if (enterOrder['orderId'] ~= nil and takeProfitOrder['orderId'] ~= nil and enterOrder['status'] == 'complete' and takeProfitOrder['status'] == 'created') then
        local stopPrice = signal.checkStopPrice(object.graph, object.indicator, object.indicator2, globalStopPrice, enterOrder['operation']);

        if (stopPrice ~= nil) then
            order.delete(takeProfitOrder['ticker'], object.account, object.classCode, takeProfitOrder['transId'], takeProfitOrder['orderId'], object.clientCode);
                
            sleep(3000);

            if (enterOrder['operation'] == 'B') then
                order.submit('M', 'Sell', object.account, object.ticker, object.classCode, stopPrice, object.quantity, object.clientCode, object.precision);
            end;

            if (enterOrder['operation'] == 'S') then
                order.submit('M', 'Buy', object.account, object.ticker, object.classCode, stopPrice, object.quantity, object.clientCode, object.precision);
            end;

            sleep(5000);
        end;
    end;

    if (enterOrder['orderId'] ~= nil and enterOrder['status'] == 'complete' and takeProfitOrder['orderId'] == nil and enterOrder['operation'] == 'B') then
        order.submit('L', 'Sell', object.account, object.ticker, object.classCode, enterOrder['profitPrice'], object.quantity, object.clientCode, object.precision);
        sleep(3000);
    end;

    if (enterOrder['orderId'] ~= nil and enterOrder['status'] == 'complete' and takeProfitOrder['orderId'] == nil and enterOrder['operation'] == 'S') then
        order.submit('L', 'Buy', object.account, object.ticker, object.classCode, enterOrder['profitPrice'], object.quantity, object.clientCode, object.precision);
        sleep(3000);
    end;

    if (enterOrder['orderId'] ~= nil and enterOrder['status'] == 'created' and (os.time() - enterOrder['updatedAt']) > 180) then
        order.delete(enterOrder['ticker'], object.account, object.classCode, enterOrder['transId'], enterOrder['orderId'], object.clientCode);
        sleep(3000);
    end;
end;

function OnInit()
    db.open(slotId);
    enterOrder = db.restoreSlotState();
    if (enterOrder['orderId'] ~= nil) then
        globalStopPrice     = enterOrder['stopPrice'];
        globalProfitPrice   = enterOrder['profitPrice'];
        globalPurchasePrice = enterOrder['purchasePrice'];
    end;
end;

function OnStop()
    db.close();
    isRun = false;
end;

function OnOrder(orderObj)
    if (bit.band(orderObj.flags, 0x1) == 0x1) then
        status="created";
    end;

    if (bit.band(orderObj.flags, 0x2) == 0x2) then
        status="deleted";
    end;

    if (bit.band(orderObj.flags, 0x2) == 0x0 and bit.band(orderObj.flags, 0x1) == 0x0) then
        status="complete";
    end;

    if (bit.band(orderObj.flags, 0x400) == 0x400) then
        status="rejected";
    end;

    if (bit.band(orderObj.flags, 0x800) == 0x800) then
        status="rejected_limit_control";
    end;
    
    operation = 'B';
    if (bit.band(orderObj.flags, 0x4) == 0x4) then 
        operation = 'S';
    end;

    local p = {
        ['orderId']       = tostring(orderObj.order_num),
        ['transId']       = tostring(orderObj.trans_id),
        ['quantity']      = tostring(orderObj.qty),
        ['purchasePrice'] = globalPurchasePrice,
        ['typeOrder']     = 'order',
        ['operation']     = tostring(operation),
        ['status']        = status,
        ['updatedAt']     = os.time(),
        ['ticker']        = tostring(orderObj.sec_code),
        ['stopPrice']     = globalStopPrice,
        ['profitPrice']   = globalProfitPrice,
    };
        saveProgress(p);

    -- if (order.checkTransId(tostring(orderObj.trans_id))) then
    --     saveProgress(p);
    -- end;
end;

function saveProgress(params)

    if (prev['enterOrder'] ~= nil and prev['enterOrder'].orderId == params['orderId']) then
        return false;
    end;

    if (prev['takeProfitOrder'] ~= nil and prev['takeProfitOrder'].orderId == params['orderId']) then
        return false;
    end;

    if (prev['stopOrder'] ~= nil and prev['stopOrder'].orderId == params['orderId']) then
        return false;
    end;

    if (params['typeOrder'] == 'order') then

        if (enterOrder['orderId'] == nil) then
            enterOrder = params;
            db.lockSlot(enterOrder);
            db.addTransaction(params);
        end;

        if (enterOrder['orderId'] ~= nil and enterOrder['orderId'] == params['orderId']) then
            enterOrder = params;
            db.lockSlot(enterOrder);
            db.addTransaction(params);
        end;

        if (enterOrder['orderId'] ~= nil and enterOrder['orderId'] == params['orderId'] and enterOrder['status'] == 'deleted') then
            db.unlockSlot();
            db.addTransaction(params);
            reset();
        end;


        if (enterOrder['orderId'] ~= nil and enterOrder['orderId'] ~= params['orderId'] and enterOrder['status'] == 'complete' and takeProfitOrder['orderId'] == nil) then
            takeProfitOrder = params;
            db.addTransaction(params);
        end;

        if (takeProfitOrder['orderId'] ~= nil and takeProfitOrder['orderId'] == params['orderId']) then
            takeProfitOrder = params;
            db.addTransaction(params);
        end;

        if (takeProfitOrder['orderId'] ~= nil and takeProfitOrder['orderId'] == params['orderId'] and takeProfitOrder['status'] == 'complete') then
            db.unlockSlot();
            db.addTransaction(params);
            reset();
        end;


        if (takeProfitOrder['orderId'] ~= nil and takeProfitOrder['orderId'] ~= params['orderId'] and takeProfitOrder['status'] == 'deleted' and stopOrder['orderId'] == nil) then
            stopOrder = params;
            db.addTransaction(params);
        end;

        if (stopOrder['orderId'] ~= nil and stopOrder['orderId'] == params['orderId']) then
            stopOrder = params;
            db.addTransaction(params);
        end;

        if (stopOrder['orderId'] ~= nil and stopOrder['orderId'] == params['orderId'] and stopOrder['status'] == 'complete') then
            db.unlockSlot();
            db.addTransaction(params);
            reset();
        end;

    end;

end;

function reset()
    if (enterOrder['orderId'] ~= nil) then
        local copy = arr.deepcopy(enterOrder);
        prev['enterOrder'] = copy;
    end;

    if (stopOrder['orderId'] ~= nil) then
        local copy = arr.deepcopy(stopOrder);
        prev['stopOrder'] = copy;
    end;

    if (takeProfitOrder['orderId'] ~= nil) then
        local copy = arr.deepcopy(takeProfitOrder);
        prev['takeProfitOrder'] = copy;
    end;

    enterOrder = {};
    stopOrder  = {};
    takeProfitOrder  = {};
end;