db = require('db');
dbg = require('tools/dbg');
order = require('order');

function getScriptPath()
	return io.popen"cd":read'*l';
end;

function sendTransaction(trs)
	print(dbg.dump(trs));
	return nil;
end;

db.addTransaction('testslot', {['test'] = 'ok'});
db.lockSlot('testslot',  {['test'] = 'ok'});
db.lockSlot('testslot1', {['test'] = 'ok'});
db.unlockSlot('testslot1');

lock = db.restoreSlot('testslot');

print(dbg.dump(lock));

db.log('testslot', "Test test test");

order.submit('b', 'L01-test', 'SBER', 'TQBR', '1000', '12396100');
order.delete('SBER', 'L01-test', 'TQBR', '123', '345', '12396100');
